<?php 
	namespace Bootstrap;

	class BootstrapForm
	{
		public function __constructor(){

		}

		public function generateLogin($args) {
			foreach ($args as $key => $value) {
				$this->$key = $value;
			}
			echo '<form action="'.$this->action.'" class="'.$this->formClass.'" id="'.$this->formID.'" method="'.$this->method.'" '.$this->enctype().'>';
			echo '<div class="form-group">
				 	<label for="username">Username</label>
				 	<input type="text" id="username" class="form-control" placeholder="Username">
				</div>
				<div class="form-group">
					<label for="password">Password</label>
				 	<input type="password" id="password" class="form-control" placeholder="Password">
				</div>
				<div class="form-group">
				 	<input type="submit" id="submitLogin" class="btn btn-primary" value="Login">
				 	<a href="'.$this->registerLink.'" class="btn btn-success">Register</a><hr>
				 	<div class="form-group">
						<a href="'.$this->forgotPasswordLink.'">Forgot password?</a>
				 	</div>
				</div>';
			echo '</form>';
		}

		public function setForm($args)
		{
			foreach ($args as $key => $value) $this->$key = $value;
		}

		public function generateForm($args) {
			echo '<form action="'.$this->action.'" class="'.$this->formClass.'" id="'.$this->formID.'" method="'.$this->method.'" '.$this->enctype().'>
			';
			foreach ($args as $key => $value) {
				if(is_array($value))
				{
					$placeholder = ucfirst(implode(" ", explode("_",$key)));
					// for elements
					echo '<div class="form-group">
					';

					if($key!='submit')
						echo '<label for="'.$key.'">'.$placeholder.'</label>
					';
					echo '<input
							type="'.$value[0].'"
							name="'.$key.'"
							id="'.$key.'"
							class="'.$value[1].'"
							value="'.$value[2].'"
							placeholder="'.$placeholder.'"
						>
						';
					echo '</div>
					';
				}
			}
			echo '</form>
			';
		}



		public function generateForgotPassword($args) {
			foreach ($args as $key => $value) {
				$this->$key = $value;
			}
			echo '<form action="'.$this->action.'" class="'.$this->formClass.'" id="'.$this->formID.'" method="'.$this->method.'" '.$this->enctype().'>';
			echo '<div class="form-group">
				 	<label for="email">Email</label>
				 	<input type="email" id="email" class="form-control" placeholder="Email">
				</div>
				<div class="form-group">
				 	<input type="submit" id="submitFP" class="btn btn-primary" value="Login"><hr>
				 	<div class="form-group">
						<a href="'.$this->registerLink.'" class="">Register</a> or 
				 		<a href="'.$this->loginLink.'" class="">Login</a>
				 	</div>
				</div>';
			echo '</form>';
		}

		private function enctype(){
			if($this->enctype){
				return 'enctype="multipart/form-data"';
			}
			else return "";
		}



		// variables

// form variables
		private $action = "#";
		private $method = "GET";
		private $formClass = "offset-md-4 col-md-4 col-sm-12 mt-4";
		private $formID = "form";
		private $enctype = false;
		private $registerLink = "register";
		private $loginLink = "login";
		private $forgotPasswordLink = "forgotpassword";
	}
 ?>
