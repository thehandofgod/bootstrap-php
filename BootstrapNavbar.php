<?php 

namespace Bootstrap;

	class BootstrapNavbar
    {
		public function __constructor(){
		}



		public function generate(){
			echo '<nav class="navbar navbar-expand-md '.$this->navbarClass.'">
				  <!-- Brand -->
				  <a class="navbar-brand" href="'.$this->setBrandLink.'">'.$this->brandName.'</a>';

			echo '<!-- Toggler/collapsibe Button -->
				  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#'.$this->collapsibleID.'">
				    <span class="navbar-toggler-icon"></span>
				  </button>';

			echo '<!-- Navbar links -->
				  <div class="collapse navbar-collapse" id="'.$this->collapsibleID.'">
				    <ul class="navbar-nav">';
			foreach ($this->navLinks as $key => $value) {
				if(!is_array($value)) {
					echo '<li class="nav-item"><a class="nav-link" href="'.$value.'">'.$key.'</a></li>
                    ';
				}
				else {
					echo '<!-- Dropdown -->
						<li class="nav-item dropdown">
						  <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">'.$key.'</a>
						  <div class="dropdown-menu">';
						    foreach ($value as $innerKey => $innerValue) {
						    	echo '<a class="dropdown-item" href="'.$innerValue.'">'.$innerKey.'</a>
                                ';
						    }
					echo '</div>
						</li>';
				}
			}
			echo 	'</ul>
				  </div>
				</nav> ';
		}


// variables

		private $brandName = "The Brand";
		private $brandLink = "#";
		private $collapsibleID = "collapsibleNavbar";
		private $navbarClass = " bg-dark navbar-dark";
		private $navbarDropID = "navbardrop";
		public $navLinks = ['First'=>'#'];





        public function setNavbar($args){
            foreach ($args as $key => $value) $this->$key = $value;
        }
        
        public function setNavLinks($navLinks)
        {
            $this->navLinks = $navLinks;

            return $this;
        }
}
// end class 




?>
