<?php 

	namespace Bootstrap;

	class BootstrapUtilities 
	{
		
		function __construct()
		{
			
		}

		public function loadViewportMeta(){
			echo '<meta name="viewport" content="width=device-width, initial-scale=1.0">
			';
		}

		public function loadCDN(){
			echo '<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
			';
			echo '<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
			';
			echo '<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
			';
			echo '<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
			';
		}

		public function image($src, $alt = 'New Image', $class = ""){
			echo '<img
				src="'.$src.'"
				alt="'.$alt.'"
				class="'.$class.'"
			>';
		}

// for table

		public function setTableHead($args){
			$return = "";
			$return .= '<tr>
			';
			foreach ($args as $key => $value) {
				$return .= '<th>
				';
				$return .= ucfirst($value);
				$return .= '
				</th>
				';

			}
			$return .= '</tr>
			';
			$this->tableHead = $return;
		}

		public function setTable($args){
			foreach ($args as $key => $value) $this->$key = $value;
		}

		public function generateTable($args){
			$return = "";
			foreach ($args as $key => $value) {
				$return .= '<tr>
				';
				foreach ($value as $inner_key => $inner_value) {
					$return .= '<td>
					';
					$return .= ucfirst($inner_value);
					$return .= '
					</td>
					';

				}
				$return .= '</tr>
				';
			}
			echo '<table id="'.$this->tableID.'" class="'.$this->tableClass.'">
			';
			echo $this->tableHead;
			echo $return;
			echo '</table>
			';
		}
 	protected $tableHead = '<tr><th>Sno</th></tr>';
 	protected $tableClass = 'table';
 	protected $tableID = '';


	}


 ?>