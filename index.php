<?php 
error_reporting(1);
require_once("BootstrapNavbar.php");
require_once("BootstrapForm.php");
require_once("BootstrapUtilities.php");
use Bootstrap\BootstrapNavbar;
use Bootstrap\BootstrapForm;
use Bootstrap\BootstrapUtilities;
$bsNav = new BootstrapNavbar;
$bsForm = new BootstrapForm;
$bsUtilities = new BootstrapUtilities;

 ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<?php $bsUtilities->loadViewportMeta(); $bsUtilities->loadCDN(); ?>
</head>
<body>
<?php 

// first navbar

	$bsNav->setNavbar([
		'brandName' => 'Titla Gang',
		'brandLink' => '#',
		'collapsibleID' => 'nav1',
		'navbarClass' => 'bg-primary navbar-dark',
	]);
	$bsNav->setNavLinks([
		'First' => '#',
		'Second' => '#',
		'Third' => '#',
		'Fourth' => [
			'First' => '#',
			'Second' => '#',
			'Third' => '#'
		]
	]);
	$bsNav->generate(); 


	$bsNav->setNavbar([
		'brandName' => '(c) DeadpooL',
		'brandLink' => 'http://killstreak.pe.hu/',
		'navLinks' => [],
		'navbarClass' => 'bg-dark navbar-dark fixed-bottom',
	]);
	$bsNav->generate();



	// form
	// $bsForm->generateLogin(['registerLink'=>'http://killstreak.pe.hu']);
	// $bsForm->generateForgotPassword(['registerLink'=>'http://killstreak.pe.hu']);
	$bsForm->setForm([
		'action'=>'somePage',
		// 'formClass'=>'col-sm-12 mt-4 mb-4'
	]);
	$bsForm->generateForm([
		'first_name' => ['text','form-control',''],
		'last_name' => ['text','form-control',''],
		'submit' => ['submit','btn btn-primary','Submit']
	]);
	// $bsUtilities->image("image.png","New Image");




// optional
	$bsUtilities->setTable([
		'tableID' => "",
		'tableClass' => "table"
	]);
// mandatory
 	$bsUtilities->setTableHead(['sno',"Name",'something']);
 	$bsUtilities->generateTable([
 		[1, "Name", 'active']
 	]);
 ?>
</body>
</html>